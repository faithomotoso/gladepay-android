package com.gladepay.android.request;

import com.gladepay.android.Charge;
import com.gladepay.android.exceptions.ChargeException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;


/**
 * Charge Request Body
 */
public class CardChargeRequestBody extends BaseRequestBody {

    private static final String FIELD_CLIENT_FIRSTNAME = "firstname";
    private static final String FIELD_CLIENT_LASTNAME = "lastname";
    private static final String FIELD_LAST4 = "last4";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_AMOUNT = "amount";
    private static final String FIELD_REFERENCE = "reference";
    private static final String FIELD_METADATA = "metadata";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_CARD_NO = "card_no";

    @SerializedName(FIELD_LAST4)
    private final String last4;

    @SerializedName(FIELD_CLIENT_FIRSTNAME)
    private final String firstname;

    @SerializedName(FIELD_CLIENT_LASTNAME)
    private final String lastname;

    @SerializedName(FIELD_AMOUNT)
    private final String amount;

    @SerializedName(FIELD_REFERENCE)
    private final String reference;
    
    
    private final String expiry_year;
    
    private final String expiry_month;
    private final String cvc;
    private String pin;

    @SerializedName(FIELD_METADATA)
    private String metadata;

    @SerializedName(FIELD_CURRENCY)
    private String currency;

    @SerializedName(FIELD_EMAIL)
    private String email;


    @SerializedName(FIELD_CARD_NO)
    private String card_no;


    private HashMap<String, String> additionalParameters;

    public CardChargeRequestBody(Charge charge) {
        if(charge == null) {
            throw new ChargeException("Please Provide A Valid And NonNull Charge");
        }
        this.setDeviceId();
        this.last4 = charge.getCard().getLast4digits();
        this.email = charge.getEmail();
        this.amount = Integer.toString(charge.getAmount());
        this.reference = charge.getReference();
        this.metadata = charge.getMetadata();
        this.additionalParameters = charge.getAdditionalParameters();
        String tempName = charge.getCard().getName() == null? "" : charge.getCard().getName();
        String[] names_part = tempName.split(" ");
        if (names_part.length > 1) {
            this.firstname = names_part[0];
            this.lastname = names_part[1];
        } else {
            this.firstname = names_part[0];
            this.lastname = "";
        }
        this.card_no = charge.getCard().getNumber();
        this.expiry_month = Integer.toString(charge.getCard().getExpiryMonth());
        this.expiry_year = Integer.toString(charge.getCard().getExpiryYear());
        this.cvc = charge.getCard().getCvc();
        this.pin = "3310";
    }

    public CardChargeRequestBody addPin(String pin) {
//        this.handle = Crypto.encrypt(pin);
        this.pin = pin;
        return this;
    }


    /**
     *
     * @return
     */
    @Override
    public JsonObject getInitiateParamsJsonObjects(){

        Gson gson2 = new Gson();

        JsonObject step1_initiate_jsonob = new JsonObject();
        step1_initiate_jsonob.addProperty("action", "initiate");
        step1_initiate_jsonob.addProperty("paymentType", "card");

//        step1_initiate_jsonob.addProperty("amount", 10000);


        step1_initiate_jsonob.addProperty("country", "NG");
        step1_initiate_jsonob.addProperty("currency", "NGN");

        JsonObject userJson = new JsonObject();
        userJson.addProperty("firstname", "Joe");
        userJson.addProperty("lastname", "Doe");

        userJson.addProperty("email", "hello@example.com");
        userJson.addProperty("ip", "192.168.33.10");
        userJson.addProperty("fingerprint", "cccvxbxbxb");

        if(amount != null) {
            step1_initiate_jsonob.addProperty("amount", this.amount);
        }else{
            throw new ChargeException("Charge Card Amount Required");
        }

//        userJson.addProperty("firstname", this.firstname);
//        userJson.addProperty("lastname", this.lastname);


        if(email != null) {
            userJson.addProperty("email", this.email);
        }else{
            throw new ChargeException("Email Has To Be Provided");
        }


        JsonObject cardJson = new JsonObject();

//        cardJson.addProperty("card_no", "5438898014560229");
//        cardJson.addProperty("expiry_month", "09");
//        cardJson.addProperty("expiry_year", "19");
//        cardJson.addProperty("ccv", "798");
//        cardJson.addProperty("pin", "3310");

        cardJson.addProperty("card_no", this.card_no);
        cardJson.addProperty("expiry_month", this.expiry_month);
        cardJson.addProperty("expiry_year", this.expiry_year);
        cardJson.addProperty("ccv", this.cvc);
        cardJson.addProperty("pin", this.pin);

        step1_initiate_jsonob.add("user", userJson);
        step1_initiate_jsonob.add("card", cardJson);

        return step1_initiate_jsonob;
    }

    /**
     *
     * @param action
     * @param txnRef
     * @param otp
     * @return
     */
    public JsonObject getParamsJsonObjects(String action, String txnRef, String otp){


        if(action.equalsIgnoreCase("validate")){
            JsonObject validate_jsonob = new JsonObject();

            validate_jsonob.addProperty("action", "validate");
            validate_jsonob.addProperty("txnRef", txnRef);
            validate_jsonob.addProperty("otp",otp);

            return validate_jsonob;

        }else if(action.equalsIgnoreCase("verify")){
            JsonObject verify_jsonob = new JsonObject();

            verify_jsonob.addProperty("action", "verify");
            verify_jsonob.addProperty("txnRef", txnRef);

            return verify_jsonob;
        }else{
            return null;
        }

    }

}
